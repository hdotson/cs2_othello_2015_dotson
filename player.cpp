#include "player.h"

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;
	
     // First, set up a new board.
     board = new Board();
     
     // Next, intitialize the sides to the correct colors.
     mySide = side;
     if(mySide == BLACK)
     {
		 otherSide = WHITE;
	 }
     else
     {
		 otherSide = BLACK;
	 }
     
}

/*
 * Destructor for the player.
 */
Player::~Player() {
	delete board;
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
	int pieces = board->countBlack() + board->countWhite();
	bool recurse = false;
	//int depth = 0;
	//int maxDepth = 1;
	// Pieces allows us a counter to start using the minimax algorithm
	// at a certain time. If pieces is less than 4, then the minimax
	// algorithm will be used the entire game.
	if(recurse)
	{
		/*MoveAndScore *chosen = 
				recurseMiniMaxMove(opponentsMove, board, depth, maxDepth);
		return chosen->move; */
	}
	else if(testingMinimax || pieces > 0)
	{
		return miniMaxMove(opponentsMove, msLeft);		
	}
	else
	{
		return randomMove(opponentsMove, msLeft);	
	}
    return NULL;
}

/*
 * This function performs a random, valid move. */
 Move *Player::randomMove(Move *opponentsMove, int msLeft) {
		// First, we want to process the opponet's move. 
		if(opponentsMove != NULL)
		{
			board->doMove(opponentsMove, otherSide);
		}

		// Next, we look at our possible moves, and pick a random one.
		vector<Move*> moves = getMoves(board, mySide);
		if(moves.size() > 0)
		{
			// Pick a random move from the avaliable moves
			int randNum = rand() % moves.size();
			Move *current = moves[randNum];
			board->doMove(current, mySide);
			return current;
		}
	return NULL;
}

/*
 * The code to execute to perform a 2-ply depth minimax algorithm.
 * This function takes an opponent's move and msLeft, and returns
 * the next move for a side to perform.*/
 Move *Player::miniMaxMove(Move *opponentsMove, int msLeft) 
 {
		// First, we want to process the opponet's move. 
		if(opponentsMove != NULL)
		{
			board->doMove(opponentsMove, otherSide);
		}

		// Next, we look at our possible moves, and construct the board
		// that would follow from that move.

		int minGain = -100; // Minimum total gain, set low initially
		int branch = 0; // Which branch of the tree that will be followed
		
		// The best score after one move (used if a full 2-ply tree
		// will not be able to be constructed)
		int oneScore = -100; 
		vector<Move*> moves = getMoves(board, mySide);
		//fprintf(stderr, "size: %d\n", moves.size()); // For debugging
		if(moves.size() > 0)
		{
			for(unsigned int i = 0; i < moves.size(); i++)
			{
				// Minimum score gain for a branch (or initial move),
				// set high initially
				int minBranchGain = 100; 
				// Construct the board following a move.
				Board *futureBoard = board->copy();
				futureBoard->doMove(moves[i], mySide);
				int moveScore = complexMoveScore(futureBoard);
				//fprintf(stderr, "my X: %d\n", moves[i]->x); // For debugging
				//fprintf(stderr, "my Y: %d\n", moves[i]->y); // For debugging
				vector<Move*> opposingMoves = getMoves(futureBoard, otherSide);
				// If the opponent can move, we consider the next level
				// of the tree.
				if(opposingMoves.size() > 0)
				{
					for(unsigned int k = 0; k < opposingMoves.size(); k++)
					{
						// Construct the boards following an opposing move.
						Board *futureBoard2 = futureBoard->copy();
						futureBoard2->doMove(opposingMoves[k], otherSide);
						// Current board score is your pieces - their pieces
						int curScore = complexMoveScore(futureBoard2);

						//fprintf(stderr, "curScore: %d\n", curScore);
						// If the score from this move was less than 
						// the previous minimum, set the min to curScore.
						if(curScore < minBranchGain)
						{
							minBranchGain = curScore;
							
						}
					}
					// If the minimum gain of this branch is larger than
					// that of the previous minimum gain, this is the 
					// best branch so far.
					if(minBranchGain > minGain)
					{
						minGain = minBranchGain;
						branch = i;
						//fprintf(stderr, "minGain: %d\n", minGain);
						//fprintf(stderr, "branch: %d\n", branch);
					}
				}else
				{
					// If we cannot go 2 layers down, the current
					// board state is relevant.
					if(moveScore > oneScore)
					{
						oneScore = moveScore;
						branch = i;
					}
				}
			}
			// Pick our move and update the board state.
			Move *current = moves[branch];
			board->doMove(current, mySide);
			return current;
		}
	return NULL;
}




/* This is a recursive minimax algorithm.*/
/*MoveAndScore *Player::recurseMiniMaxMove(Move *opponentsMove, Board *board, int depth, int maxDepth)
 {
	 Move *bestMove, *chosenMove;
	 MoveAndScore *result;
	 int chosenScore = -1000, bestScore = -1000;
	 // First, we want to process the opponet's move. 
	if(opponentsMove != NULL)
	{
		board->doMove(opponentsMove, otherSide);
	}
	// Next, we look at our possible moves, and construct the board
	// that would follow from that move.	
	if(depth == maxDepth)
		chosenScore = complexMoveScore(board);
	else
	{
		vector<Move*> moves = getMoves(board, mySide);
		if(moves.size() < 1)
		{
			chosenScore = complexMoveScore(board);
		}else
		{
			for(unsigned int k = 0; k < moves.size(); k++)
			{
				bestScore = 1000;
				// Construct the boards following a move.
				Board *futureBoard = board->copy();
				futureBoard->doMove(moves[k], mySide);
				MoveAndScore result = recurseMiniMaxMove(futureBoard, 
														depth+1, depth);
				//fprintf(stderr, "curScore: %d\n", curScore);
				// If the score from this move was less than 
				// the previous minimum, set the min to curScore.
				if(result.score > bestScore)
				{
					bestScore = result.score;
					bestMove = result.move;
					
				}
			}
			chosenScore = bestScore;
			chosenMove = bestMove;

			
		}
	}
	result.move = chosenMove;
	result.score =chosenScore
	return result;
 } */

/*
 * Returns a score for a move based on a very simple heurisitc, 
 * the score of the board after that move. */
int Player::simpleMoveScore(Board *board)
{
	return board->count(mySide)-board->count(otherSide);
}

/*
 * A complex heuristic for a move score based on "An Analysis of 
 * Heuristics in Othello", by Sannidhanam and Annamalai, University
 * of Washington. It involves calculating several heuristics , such
 * as coin parity. */
int Player::complexMoveScore(Board *board)
{
	int myPieces = 0, otherPieces = 0, myMoves = 0, 
	otherMoves = 0,total = 0;
	double parity = 0, mobility = 0, corner = 0, near = 0, side = 0;
	/* Coin parity heuristic */
	/* This takes into account the number of pieces that each side
	 * has at a certain board state. */
	myPieces = board->count(mySide);
	otherPieces = board->count(otherSide);
	parity = 100*((double)myPieces - otherPieces)/(myPieces + otherPieces);
	/* Mobility heuristic */
	/* This takes into account the avaliable moves for each side.*/
	myMoves = getMoves(board, mySide).size();
	otherMoves = getMoves(board, otherSide).size();
	if((myMoves + otherMoves) != 0)
		mobility = 100*((double)myMoves - otherMoves)/(myMoves + otherMoves);
	/* Corner heuristic*/
	/* This takes into account the corner, near corner,
	 * and side pieces. */
	corner =  cornerScore(board);
	near = nearCornerScore(board);
	side = sideScore(board);
	/*fprintf(stderr, "parity: %f ", parity);
	fprintf(stderr, "mobility: %f ", mobility);
	fprintf(stderr, "corner: %f ", corner);
	fprintf(stderr, "near: %f\n", near); //For debugging */
	/* All of the heuristics are positive except for the locations 
	 * near the corners.*/
	total = round(parity*0.25 + mobility*0.10 + side*0.20
					+ corner*0.40 - near*0.10);
	/*fprintf(stderr, "total: %d\n", total);*/
	return total;
}

/* A function to evaluate the corner score of a board. */
double Player::cornerScore(Board *board)
{
	int myCorners = 0, otherCorners = 0;
	double corner = 0;
	if(board->get(mySide, 0, 0))
		myCorners++;
	else if(board->get(otherSide, 0, 0))
		otherCorners++;
	else if(board->get(mySide, 0, 7))
		myCorners++;
	else if(board->get(otherSide, 0, 7))
		otherCorners++;
	else if(board->get(mySide, 7, 0))
		myCorners++;
	else if(board->get(otherSide, 7, 0))
		otherCorners++;
	else if(board->get(mySide, 7, 7))
		myCorners++;
	else if(board->get(otherSide, 7, 7))
		otherCorners++;
	corner = 100*((double)myCorners - otherCorners)/4; 
	// Since there are 4 corners
	return corner;
}

/* A function to evaluate the near corner score of a board, since 
 * spaces near corners are dangerous to play if the corners are
 * open. */
double Player::nearCornerScore(Board *board)
{
	int myNear = 0, otherNear = 0;
	double near = 0;
	/* Near corners are only bad if you don't have that corner*/
	if(board->isEmpty(mySide, 0, 0))
	{
		if(board->get(mySide, 0, 1))
			myNear++;
		else if(board->get(otherSide, 0, 1))
			otherNear++;
		else if(board->get(mySide, 1, 0))
			myNear++;
		else if(board->get(otherSide, 1, 0))
			otherNear++;
		else if(board->get(mySide, 1, 1))
			myNear++;
		else if(board->get(otherSide, 1, 1))
			otherNear++;
	}
	if(board->isEmpty(mySide, 0, 7))
	{
		if(board->get(mySide, 0, 6))
			myNear++;
		else if(board->get(otherSide, 0, 6))
			otherNear++;
		else if(board->get(mySide, 1, 7))
			myNear++;
		else if(board->get(otherSide, 1, 7))
			otherNear++;
		else if(board->get(mySide, 1, 6))
			myNear++;
		else if(board->get(otherSide, 1, 6))
			otherNear++;
	}
	if(board->isEmpty(mySide, 7, 0))
	{
		if(board->get(mySide, 7, 1))
			myNear++;
		else if(board->get(otherSide, 7, 1))
			otherNear++;
		else if(board->get(mySide, 6, 0))
			myNear++;
		else if(board->get(otherSide, 6, 0))
			otherNear++;
		else if(board->get(mySide, 7, 1))
			myNear++;
		else if(board->get(otherSide, 7, 1))
			otherNear++;
	}
	if(board->isEmpty(mySide, 7, 7))
	{
		if(board->get(mySide, 6, 7))
			myNear++;
		else if(board->get(otherSide, 6, 7))
			otherNear++;
		else if(board->get(mySide, 7, 6))
			myNear++;
		else if(board->get(otherSide, 7, 6))
			otherNear++;
		else if(board->get(mySide, 6, 6))
			myNear++;
		else if(board->get(otherSide, 6, 6))
			otherNear++;
	}
	near = 100*((double)myNear - otherNear)/12;	
	//Since there are 12 near corners
	return near;
}

/* A function to side pieces, since these are morevalubale than other
 * pieces. */
double Player::sideScore(Board *board)
{
	int mySides = 0, otherSides = 0;
	double side = 0;
	for(int i = 2; i < 6; i++)
	{
		if(board->get(mySide, 0, i))
			mySides++;
		else if(board->get(otherSide, 0, i))
			otherSides++;
		if(board->get(mySide, i, 0))
			mySides++;
		else if(board->get(otherSide, i, 0))
			otherSides++;
		if(board->get(mySide, i, 7))
			mySides++;
		else if(board->get(otherSide, i, 7))
			otherSides++;
		if(board->get(mySide, 7, i))
			mySides++;
		else if(board->get(otherSide, 7, i))
			otherSides++;
	}

	side = 100*((double)mySides - otherSides)/16;	
	//Since there are 16 side spots that are not corners or near corners
	return side;
}

/*
 * Given a board and a side, this function loops through the positions 
 * on the board, and returns a vector of the valid moves on the board\
 * for that particular side.
 * */
vector<Move*> Player::getMoves(Board *board, Side side)
{
	vector<Move*> moves;
	// Loop through each location on the board, row by row.
	for(int r = 0; r < 8; r++)
	{
		for(int c = 0; c < 8; c++)
		{
			Move *current = new Move(r, c);
			// If the current move is valid, store it.
			if(board->checkMove(current, side))
			{
				moves.push_back(current);
			}
		}
	}
	return moves;
}
