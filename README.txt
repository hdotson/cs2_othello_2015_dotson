Othello AI by Hannah Dotson

I worked solo for this project. 

I made several changes to my AI from the simple random player to make it 
tournament-worthy. 

The first change I made was making a simple heuristic function. This function simply
takes a board state and calculates a score of the player's pieces minus the other
side's pieces. Using this to pick the next move was definite improvement over a
random selection, but not by that much. 

The next change I made was implementing a 2-ply depth minimax decision tree, as
described in assignment 9. For this, I at first used the simple heuristic function. This
was done very simply, just using a pair of nested for loops. 

Finally, I changed the heuristic in the minimax tree to a much more complex one.
The new complex heuristic takes into account not only the score of the
player's pieces minus the other side's pieces, but also mobility, corners, near
corners, and sides. Corners and sides are advantageous, while near corners are not.
In addition, we want higher mobility. I essentially made a score for these, based on
the paper "An Analysis of Heuristics in Othello" by Sannidhanam and Annamalai from
the University of Washington. Unfortunately, I did not have enough time to
implement the stability heuristic they mentioned in the paper. Weighting for the
various parts of my complex heuristic were based roughly on the paper, but also
tweaked through testing against both ConstantTimePlayer and BetterPlayer. 

For all of the above, the implementations are commented in player.cpp. 

These changes were enough to consistently beat both ConstantTimePlayer and
BetterPlayer. I think that the heuristic I used was particularly strong, since my
program is able to beat BetterPlayer, which recursively evaluates moves to 
depth 3. My heuristic function is likely significantly better than BetterPlayer's,
which I think will give me an advantage. I have not had any memory or time issues,
and did not have to specificially implement anything to account for these
parameters. I think my strategy will work because it is simple, but has a very good
heuristic for evaluating board state that gives it a strong advantage over other
similar AIs. 

I wanted to change my minmax decision tree to a recursive form, so that it would
be easier to implement a deeper tree. However, I did not have time to fully debug this,
so it is still in my code, but commented out. Being able to go more than 2 layers
deep in the minimax tree would help significantly in picking the next room. 
Implementing the stability heuristic in the paper would also have been useful to
get an even better board position score. 