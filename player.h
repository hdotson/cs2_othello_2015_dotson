#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include <cstdlib>
#include <vector>
#include <stdio.h>
#include "common.h"
#include "board.h"
#include <math.h>
using namespace std;

struct MoveAndScore {
	Move *move;
	int score;
	};

class Player {

public:
    Player(Side side);
    ~Player();
    

    
    Move *doMove(Move *opponentsMove, int msLeft);
    vector<Move*> getMoves(Board *board, Side side);
	Move *miniMaxMove(Move *opponentsMove, int msLeft); 
	Move *randomMove(Move *opponentsMove, int msLeft); 
	MoveAndScore *recurseMiniMaxMove(Move *opponentsMove, 
	Board *board, int depth, int maxDepth);
	int simpleMoveScore(Board *board); // Simple heuristic function
	int complexMoveScore(Board *board); // Complex heuristic function
	double cornerScore(Board *board);
	double nearCornerScore(Board *board);
	double sideScore(Board *board);
    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
    Board *board; // Current board
    Side mySide;
    Side otherSide;
    
};

#endif
